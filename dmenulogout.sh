#!/bin/bash

RET=$(echo -e "shutdown\nreboot\nlogout\ncancel" | dmenu -l 5 -p "Logout")

case $RET in
	shutdown) sudo poweroff ;;
	reboot) sudo reboot ;;
	logout) xdotool key "super+shift+q" ;;
	*) ;;
esac
